#include <csignal>
#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/image_encodings.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"
#include "image_transport/image_transport.h"



void imagecallback(const sensor_msgs::ImageConstPtr &msg)
{
		//creiamo 2 immagini
		cv_bridge::CvImagePtr cv_ptrgrey=cv_bridge::toCvCopy(msg, "mono8");
		cv_bridge::CvImagePtr cv_ptrcolor=cv_bridge::toCvCopy(msg, "mono8");
	cv::Mat original = cv_ptrgrey->image;
	cv::Mat resulting = cv_ptrcolor->image;
	
	//applichiamo i filtri
	GaussianBlur(resulting, resulting, cv::Size{3, 3}, 0, 0);
	Canny(resulting, resulting, 50.0, 50.0);
	
	//mostriamo le immagini nelle finestre
	imshow("Original", original);
	imshow("Resulting", resulting);
	
	cv::waitKey(100);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "edge_extractor_node");//inizializza
	
	ros::NodeHandle nh{};
	ROS_INFO("edge_extractor_node running");
	//creiamo 2 finestre
	cv::namedWindow("Original");
	cv::namedWindow("Resulting");
	
	
	image_transport::ImageTransport img_tr{nh};
	//riceve l'immagine e chiama callback
	image_transport::Subscriber sub_image = img_tr.subscribe("/default/imagenotcompressed", 1, imagecallback);
	ros::spin();
	return 0;
}
