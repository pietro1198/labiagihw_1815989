#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "move_task");
    //istanzia client
    MoveBaseClient ac("move_base", true);
    //mi metto in attesa del server!
    while(!ac.waitForServer(ros::Duration(5.0)))
    {
        ROS_INFO("Waiting for the move_base server");
    }
	//instanziamo struttura di tipo goal
    move_base_msgs ::MoveBaseGoal goal;

    goal.target_pose.header.frame_id = "base_link" ;
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = -15.29;
    goal.target_pose.pose.position.y = 22.77;
    goal.target_pose.pose.orientation.z = 1;
    goal.target_pose.pose.orientation.w = 0;
    //inviamo il goal
    ac.sendGoal(goal);
	ROS_INFO("Goal sent : %f,%f,%f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y, goal.target_pose.pose.position.z);

    bool reached =  ac.waitForResult(ros::Duration(10.0));
    ROS_INFO("The goal state has been reached: %f",reached);
    //cancelliamo tutti i goals
    ac.cancelAllGoals();
    goal.target_pose.header.frame_id = "base_link" ;
        goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.pose.position.x = -11.89;
        goal.target_pose.pose.position.y = 22.77;
        goal.target_pose.pose.orientation.z = 1;
        goal.target_pose.pose.orientation.w = 0;
        ac.sendGoal(goal);
        ac.waitForResult();
    
    return 0;
}
